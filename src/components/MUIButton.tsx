import * as React from "react";
import Stack from "@mui/material/Stack";
import Button from "@mui/material/Button";
import DeleteIcon from "@mui/icons-material/Delete";
import SendIcon from "@mui/icons-material/Send";
import IconButton from "@mui/material/IconButton";

export default function BasicButtons() {
  return (
    <>
      <Stack spacing={2} direction="row">
        <Button variant="text">Text</Button>
        <Button variant="contained">Contained</Button>
        <Button variant="outlined">Outlined</Button>
      </Stack>
      <Stack spacing={2} direction="row">
        {/* default variant is text */}
        <Button>Primary</Button>
        <Button disabled>Disabled</Button>
        <Button href="#link">Link</Button>
        {/*we can add the href attribute in button also*/}
        <Button variant="contained" disableElevation>
          Disable elevation
        </Button>
      </Stack>
      <Stack spacing={2} direction="row">
        <Button color="secondary">Secondary</Button>
        <Button variant="contained" color="success">
          Success
        </Button>
        <Button variant="outlined" color="error">
          Error
        </Button>
      </Stack>
      <div>
        <Button variant="contained" size="small">
          Small
        </Button>
        <Button variant="contained" size="medium">
          Medium
        </Button>
        <Button variant="contained" size="large" onClick={() => alert("hello")}>
          Large
        </Button>
      </div>
      <Stack direction="row" spacing={2}>
        <Button variant="outlined" startIcon={<DeleteIcon />}>
          Delete
        </Button>
        <Button variant="contained" endIcon={<SendIcon />}>
          Send
        </Button>
        <IconButton aria-label="delete">
          <DeleteIcon />
        </IconButton>
      </Stack>
    </>
  );
}
