import { Tooltip, IconButton } from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { Box } from "@mui/system";

const MuiTooltip = () => {
  return (
    <Box sx={{ margin: 9 }}>
      <Tooltip
        title="Delete"
        // placement="right"
        placement="left"
        // placement="top"
        // placement="bottom"
        arrow
        enterDelay={500}
        leaveDelay={200}
      >
        <IconButton>
          <DeleteIcon />
        </IconButton>
      </Tooltip>
    </Box>
  );
};
export default MuiTooltip;
