import * as React from "react";
import Switch from "@mui/material/Switch";
import { Box } from "@mui/system";
import { FormControlLabel } from "@mui/material";

export default function ControlledSwitches() {
  const [checked, setChecked] = React.useState(true);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.checked);
  };

  console.log(checked, "switch value");
  return (
    <Box>
      <FormControlLabel
        control={<Switch checked={checked} onChange={handleChange} />}
        label="Dark mode"
      />
    </Box>
  );
}
