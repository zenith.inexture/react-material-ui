import { Stack, Switch } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import SaveIcon from "@mui/icons-material/Save";
import { useState } from "react";

const MuiLoadingButton = () => {
  const [isLoading, setIsLoading] = useState(false);

  return (
    <Stack direction="row" spacing={2}>
      <Switch
        checked={isLoading}
        onChange={() => setIsLoading(!isLoading)}
        name="loading"
        color="primary"
      />
      <LoadingButton variant="outlined">Submit</LoadingButton>
      <LoadingButton loading={isLoading} variant="outlined">
        Submit
      </LoadingButton>
      <LoadingButton loadingIndicator="Loading..." variant="outlined">
        Fetch data
      </LoadingButton>
      <LoadingButton
        loading={isLoading}
        loadingIndicator="Loading..."
        variant="outlined"
      >
        Fetch data
      </LoadingButton>
      <LoadingButton
        loadingPosition="start"
        startIcon={<SaveIcon />}
        variant="outlined"
      >
        Save
      </LoadingButton>
      <LoadingButton
        loading={isLoading}
        loadingPosition="start"
        startIcon={<SaveIcon />}
        variant="outlined"
      >
        Save
      </LoadingButton>
    </Stack>
  );
};

export default MuiLoadingButton;
