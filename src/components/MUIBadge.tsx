import * as React from "react";
import Badge from "@mui/material/Badge";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import { Button, ButtonGroup } from "@mui/material";
import RemoveIcon from "@mui/icons-material/Remove";
import AddIcon from "@mui/icons-material/Add";

export default function CustomizedBadges() {
  //we can use it in the e-commerce website for add to cart and remove to cart
  const [count, setCount] = React.useState(1);

  return (
    <>
      {/* we can define he position of the badge by self also */}
      <Badge badgeContent={count} color="primary" sx={{ margin: 2 }}>
        <ShoppingCartIcon color="action" />
      </Badge>
      <Badge badgeContent={count} color="secondary" max={5} sx={{ margin: 2 }}>
        <ShoppingCartIcon color="action" />
      </Badge>
      <Badge badgeContent={count} color="warning" showZero sx={{ margin: 2 }}>
        <ShoppingCartIcon color="action" />
      </Badge>
      <Badge
        badgeContent={count}
        color="secondary"
        variant="dot"
        sx={{ margin: 2 }}
      >
        <ShoppingCartIcon color="action" />
      </Badge>
      <ButtonGroup>
        <Button
          aria-label="reduce"
          onClick={() => {
            setCount(Math.max(count - 1, 0));
          }}
        >
          <RemoveIcon fontSize="small" />
        </Button>
        <Button
          aria-label="increase"
          onClick={() => {
            setCount(count + 1);
          }}
        >
          <AddIcon fontSize="small" />
        </Button>
      </ButtonGroup>
    </>
  );
}
