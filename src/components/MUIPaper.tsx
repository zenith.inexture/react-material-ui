import * as React from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import { Typography } from "@mui/material";

export default function SimplePaper() {
  return (
    // one type of shadowed div tag for display the verious chid component
    <Box
      sx={{
        display: "flex",
        flexWrap: "wrap",
        "& > :not(style)": {
          m: 1,
          width: 128,
          height: 128,
          backgroundColor: "#eeeeee",
        },
      }}
    >
      {/* like this we can write the child components into the paper also */}
      <Paper elevation={0}>
        <Typography variant="h6">hello</Typography>
      </Paper>
      <Paper />
      <Paper elevation={4} />
    </Box>
  );
}
