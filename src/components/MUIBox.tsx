import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

export default function BoxComponent() {
  return (
    //this box is render as span tag
    //box is one type of div tag that cover more then one tags in it
    <Box component="span" sx={{ p: 2, border: "1px dashed grey" }}>
      <Button>Save</Button>
      <span>hello</span>
    </Box>
  );
}
