import MUITypography from "./components/MUITypography";
import MUIButton from "./components/MUIButton";
import MUIButtonGroup from "./components/MUIButtonGroup";
import MUIToggleButton from "./components/MUIToggleButton";
import MUITextField from "./components/MUITextField";
import MUISelect from "./components/MUISelect";
import MUIRadioButtons from "./components/MUIRadioButtons";
import MUICheckbox from "./components/MUICheckbox";
import MUISwitch from "./components/MUISwitch";
import MUIRating from "./components/MUIRating";
import MUIAutocomplete from "./components/MUIAutocomplete";
import MUIBox from "./components/MUIBox";
import MUIStack from "./components/MUIStack";
import MUIGrid from "./components/MUIGrid";
import MUIPaper from "./components/MUIPaper";
import MUICard from "./components/MUICard";
import MUIAccordion from "./components/MUIAccordion";
import MUIImageList from "./components/MUIImageList";
import MUINavbar from "./components/MUINavbar";
import MUIMenu from "./components/MUIMenu";
import MUIBreadcrumbs from "./components/MUIBreadcrumbs";
import MUIDrawer from "./components/MUIDrawer";
import MUISpeedDial from "./components/MUISpeedDial";
import MUIBottomNavigation from "./components/MUIBottomNavigation";
import MUIAvatar from "./components/MUIAvatar";
import MUIBadge from "./components/MUIBadge";
import MUIList from "./components/MUIList";
import MUIChip from "./components/MUIChip";
import MUITooltip from "./components/MUITooltip";
import MUITable from "./components/MUITable";
import MUIAlert from "./components/MUIAlert";
import MUISnackbar from "./components/MUISnackbar";
import MUIDialog from "./components/MUIDialog";
import MUIProgress from "./components/MUIProgress";
import MUISkeleton from "./components/MUISkeleton";
import MUILoadingButton from "./components/MUILoadingButton";
import MUIDatePicker from "./components/MUIDatePicker";
import MUIDateRange from "./components/MUIDateRange";
import MUITabs from "./components/MUITabs";
import MUITimeLine from "./components/MUITimeLine";
import MUIMasonry from "./components/MUIMasonry";

import "./App.css";

function App() {
  return (
    <div className="App">
      {/* <MUITypography /> */}
      {/* <MUIButton /> */}
      {/* <MUIButtonGroup /> */}
      {/* <MUIToggleButton /> */}
      {/* <MUITextField /> */}
      {/* <MUISelect /> */}
      {/* <MUIRadioButtons /> */}
      {/* <MUICheckbox /> */}
      {/* <MUISwitch /> */}
      {/* <MUIRating /> */}
      {/* <MUIAutocomplete /> */}
      {/* <MUIBox /> */}
      {/* <MUIStack /> */}
      {/* <MUIGrid /> */}
      {/* <MUIPaper /> */}
      {/* <MUICard /> */}
      {/* <MUIAccordion /> */}
      {/* <MUIImageList /> */}
      {/* <MUINavbar /> */}
      {/* <MUIMenu /> */}
      {/* <MUIBreadcrumbs /> */}
      {/* <MUIDrawer /> */}
      {/* <MUISpeedDial /> */}
      {/* <MUIBottomNavigation /> */}
      {/* <MUIAvatar /> */}
      {/* <MUIBadge /> */}
      {/* <MUIList /> */}
      {/* <MUIChip /> */}
      {/* <MUITooltip /> */}
      {/* <MUITable /> */}
      {/* <MUIAlert /> */}
      {/* <MUISnackbar /> */}
      {/* <MUIDialog /> */}
      {/* <MUIProgress /> */}
      {/* <MUISkeleton /> */}
      {/* <MUILoadingButton /> */}
      {/* <MUIDatePicker /> */}
      {/* <MUIDateRange /> */}
      {/* <MUITabs /> */}
      {/* <MUITimeLine /> */}
      {/* <MUIMasonry /> */}
    </div>
  );
}

export default App;
